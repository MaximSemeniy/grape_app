require 'grape'
require 'httparty'

class App < Grape::API
  format :json

  helpers do
    def get_breeds
      json = HTTParty.get('https://api.thecatapi.com/v1/breeds').parsed_response
      breeds_arr = []
      json.each_with_index { |_, index| breeds_arr << { 'name': json[index]['name'], 'id': json[index]['id'] } }
      breeds_arr
    end

    def get_breed_info(breed)
      json = HTTParty.get("https://api.thecatapi.com/v1/breeds/search?q=#{breed}").parsed_response
      if json.size > 0
        { 'name': json[0]['name'], 'description': json[0]['description'], 'temperament': json[0]['temperament'] }
      else
        { 'error': 'Not found' }
      end
    end
  end

  get :cats do
    get_breeds
  end

  resource :cat do
    params do
      requires :breed_name, type: String
    end
    get ':breed_name' do
      get_breed_info(params[:breed_name])
    end
  end
end
